do příště:
 - závěrečná prezentace, i přes test z lingebry
 - dokončení dokumentace, vč. diagramů
 - bez odevzdání, ale musíme mít!
příští týden:
 - dostaneme feedback
 - ladíme detaily dokumentace
přespříští týden:
 - finální konzultace

prezentace:
 - 15 minut, naostro
 - může prezentovat jeden člověk i všichni
 - prezentujeme kompletní práci - provedení je na nás
   - času je málo - nebude čas ukázat vše
   - prezentujeme zadavateli/investorovi
   - prezentujeme práci, nikoliv dokument
   - problém > řešení
   - jaké jsou výstupy
   - klíčová slova: trasovatelnost, úplnost, smysluplnost, srozumitelnost
 - není to stavová prezentace - žádné harmonogramy

kompletní dokumentace:
 - vše v jednom PDF (nikoliv PDF + přílohy), se všemi náležitostmi
 - od vize až po diagramy nasazení
 - seznam formalit je na stránkách
 - přidat:
   - jedno závěrečné hodnocení za celý tým, zhodnotíme průběh práce, téma a vše okolo
   - "červené poznámky" - "Víme, že XY není úplné, ale nebyl čas/chuť apod."
   - Lessons Learned, za tým nebo za jednotlivce, je to na nás
   - každý za sebe:
     - celkové sebehodnocení
     - hodnocení práce ostatních na projektu
   - výkazy (po týdnech nebo po odevzdáních) a celkově
   - do WF napojit pozdější výstupy, pokud lze (nemáme tím trávit hodiny)
   - 
 - 

hodnocení předmětu:
 - A-F & slovní hodnocení
 - hodnocení má dvě části:
 - individuální práce
   - práce na cvičeních
   - docházka
   - úkoly
   - hodnocení v dokumentech
   - výkaz práce
 - projekt
   - kompletní dokumentace
   - konzultace
   - úplnost, smysluplnost projektu
   - "červené poznámky" +
   - stav GitLabu
   - prezentace projektu
   - kvalita oponentur
   - dochvilnost
 - hodnocení nebude vůbec přísné
 - tabluka s hodnocením nám bude nasdíleno
